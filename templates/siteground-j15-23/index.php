<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
JPlugin::loadLanguage( 'tpl_SG1' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
<!--[if lt IE 7.]>
	<script defer type="text/javascript" src="templates/<?php echo $this->template ?>/js/pngfix.js"></script>
	<![endif]-->

</head>
<body id="page_bg">
<a name="up" id="up"></a>
<div class="center" align="center">

<div id="path"><jdoc:include type="module" name="breadcrumbs" /></div>
<table cellpadding="0" cellspacing="0" id="header">
	<td class="header_l"><img src="templates/<?php echo $this->template ?>/images/left_bg.png" alt="" /></td>
	<td valign="top">
		<div class="top_bg"><img src="templates/<?php echo $this->template ?>/images/top_bg.png" alt="" /></div>
		<div id="header_bg"><a href="index.php"><?php echo $mainframe->getCfg('sitename') ;?></a></div>
		<div id="tabarea">
			<table cellpadding="0" cellspacing="0" class="pill">
				<tr>
					<td class="pill_m">
					<div id="pillmenu">
						<jdoc:include type="modules" name="user3" />
					</div>
					</td>
				</tr>
			</table>
		</div>
		<div class="header_b_bg"></div>		
	</td>
	<td class="header_r"><img src="templates/<?php echo $this->template ?>/images/right_bg.png" alt="" /></td>
</table>


<div id="wrapper">
		<div id="whitebox">
		
			<div id="whitebox_m">
				<div id="area">
						<?php if($this->countModules('left') and JRequest::getCmd('layout') != 'form') : ?>
							<div id="leftcolumn" style="float:left;">
								<jdoc:include type="modules" name="left" style="xhtml" />
								<?php $sg = 'banner'; include "templates.php"; ?>
							</div>
						<?php endif; ?>
						
						<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
						<div id="maincolumn">
						<?php else: ?>
						<div id="maincolumn_full">
						<?php endif; ?>
							<div class="nopad">
										<?php if($this->params->get('showComponent')) : ?>
											<jdoc:include type="component" />
										<?php endif; ?>
							</div>
						</div>
						<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
							<div id="rightcolumn" style="float:right;">
										<jdoc:include type="modules" name="right" style="xhtml" />								
							</div>
						<?php endif; ?>
<div class="clr"></div>
			</div>

	</div>
	<div id="bottom_bg"></div>
	<div id="footer">
		<div id="footer_l">
			<div id="footer_r">
				<p style="float:left; padding:12px 25px;">
					<jdoc:include type="modules" name="syndicate" />
				</p>
				<p style="float:right; padding:16px 25px;color:#fff;">	 			 	
					Valid <a href="http://validator.w3.org/check/referer">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
				</p>
			</div>
		</div>
	</div>			
		</div>
	</div>
	<div id="sgf"><?php $sg = ''; include "templates.php"; ?></div>
</div>
<jdoc:include type="modules" name="debug" />
</body>
</html>
