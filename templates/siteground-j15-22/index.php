<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
JPlugin::loadLanguage( 'tpl_SG1' );
include dirname( __FILE__ ).'/templateControl.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="templates/<?php echo $this->template ?>/css/template.css" type="text/css" />
<link rel="stylesheet" href="templates/<?php echo $this->template ?>/css/<?php echo $tmpl_color ?>.css" type="text/css" />
<!--[if lte IE 7]>
<link href="templates/<?php echo $this->template ?>/css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

</head>
<body id="page_bg" class="width_<?php echo $tmpl_width ?>">
<a name="up" id="up"></a>
<jdoc:include type="message" />
<div class="center" align="center">
		<div id="search">
			<?php if($this->countModules('user4')) :?>
				<jdoc:include type="modules" name="user4" />
			<?php endif; ?>
		</div>
	<div class="clr"></div>
		<div id="whitebox">
			<div id="whitebox_t">
				<div id="whitebox_tl">
					<div id="whitebox_tr"></div>
				</div>
			</div>
		</div>

		<div id="header">
			<div id="header_t">
				<div id="header_tl">
					<div id="header_tr">
						<div id="logo"><a href="index.php"><?php echo $mainframe->getCfg('sitename') ;?></a></div>
					</div>
					<div id="header_down_t">
						<div id="header_down_tl">
							<div id="header_down_tr">
								<div id="top_hr"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="tabarea">
			<div id="tab_tl">
				<div id="tab_tr">			
					<table cellpadding="0" cellspacing="0" class="pill">
						<tr>
							<td class="pill_m">
							<div id="pillmenu">
								<jdoc:include type="modules" name="user3" />
							</div>
							</td>
						</tr>
					</table>
				</div>
			</div>			
		</div>			


	<div id="main_top">
		<div id="main_top_t">
			<div id="main_top_tl">
				<div id="main_top_tr"></div>
			</div>
		</div>
	</div>
	
<div id="wrapper">	
	<div id="wrapper_l">		
		<div id="wrapper_r">
			<div id="wrapper_t_gradient">
				<div id="wrapper_l_gradient">
					<div id="wrapper_r_gradient">	
						<div id="main_hr">
			
		<div id="whitebox_m">
			<div id="area">
				<?php if($this->countModules('left') and JRequest::getCmd('layout') != 'form') : ?>
					<div id="leftcolumn" style="float:left;">
								<jdoc:include type="modules" name="left" style="rounded" />
								<?php $sg = 'banner'; include "templates.php"; ?>
					</div>
				<?php endif; ?>
								
					<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
					<div id="maincolumn">
					<?php else: ?>
					<div id="maincolumn_full">
					<?php endif; ?>
						<div class="nopad">
						<div id="pathway">
								<jdoc:include type="module" name="breadcrumbs" />
						</div>						
						
							<?php if($this->params->get('showComponent')) : ?>
								<jdoc:include type="component" />
							<?php endif; ?>
						</div>
					</div>
					<?php if($showRightColumn === true) : ?>
					<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>

					<div id="rightcolumn" style="float:right;">
						<jdoc:include type="modules" name="right" style="rounded" />								
					</div>
				<?php endif; ?>
				<?php endif; ?>
				<div class="clr"></div>
			</div>
		</div>

							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div id="footer">
		<div id="footer_l">
			<div id="footer_r">
				<div id="footer_hr">
					<p style="float:left; padding: 4px 25px;">
						<jdoc:include type="modules" name="syndicate" />
					</p>
					<p style="float:right; padding: 4px 25px;">	 			 	
						Valid <a href="http://validator.w3.org/check/referer" style="text-decoration: underline;">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer" style="text-decoration: underline;">CSS</a>.
					</p>
				</div>
			</div>
		</div>
	</div>			
	<div id="sgf"><?php $sg = ''; include "templates.php"; ?></div>
	<jdoc:include type="modules" name="debug" />	
</div>


</body>
</html>
