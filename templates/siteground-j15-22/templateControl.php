<?php
	// get the template custom parameters
	$tmpl_width = $this->params->get('widthStyle');
	if(!$tmpl_width) {
		$tmpl_width = 'fluid';
	}
	$tmpl_color = $this->params->get('templateColor');
	if(!$tmpl_color) {
		$tmpl_color = 'grey';
	}

	$showRightColumn = true;

	// disables the right column if template width is set to 'small'
	if($tmpl_width == 'small') {
		$showRightColumn = false;
	}
?>