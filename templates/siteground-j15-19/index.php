<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
JPlugin::loadLanguage( 'tpl_SG1' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" >
<head>
<jdoc:include type="head" />

<link rel="stylesheet" href="templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="templates/<?php echo $this->template ?>/css/template.css" type="text/css" />


</head>
<body id="page_bg">

<div style="position:absolute;top:244px;left:0;z-index:1;width:100%;margin:0 auto;text-align:center;"><jdoc:include type="message" /></div>
<a name="up" id="up"></a>


<div id="frame_bg">

	<div id="wrapper">
	
			<div id="whitebox">
				<table cellpadding="0" cellspacing="0" class="pill">
					<tr>
						<td class="pill_m">
							<div id="pillmenu">
								<jdoc:include type="modules" name="user3" />
							</div>
						</td>
					</tr>
				</table>
			</div>
	
			<div id="header">
				<div id="header_l">
					<div id="logo_bg">
						<div id="logo">
							<a class="logo" href="index.php"><?php echo $mainframe->getCfg('sitename') ;?></a>
					<div id="topnews">
						<jdoc:include type="modules" name="top" />
					</div>							
						</div>
						<div id="clr"></div>
					</div>
				</div>
			</div>
			
	</div>
	<div id="extras">
		<div id="search">
			<jdoc:include type="modules" name="user4" />
		</div>
		<div id="pathway">
			<jdoc:include type="module" name="breadcrumbs" />
		</div>
		<div id="clr"></div>
	</div>
	
	<div id="shodow"></div>
						
			<div id="whitebox_m">
				<div id="area">
						<?php if($this->countModules('left') and JRequest::getCmd('layout') != 'form') : ?>
							<div id="leftcolumn" style="float:left;">
								<jdoc:include type="modules" name="left" style="xhtml" />
								<?php $sg = 'banner'; include "templates.php"; ?>
							</div>
						<?php endif; ?>
						
						<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
						<div id="maincolumn">
						<?php else: ?>
						<div id="maincolumn_full">
						<?php endif; ?>
							<div class="nopad">
										<?php if($this->params->get('showComponent')) : ?>
											<jdoc:include type="component" />
										<?php endif; ?>
							</div>
						</div>
						<?php if($this->countModules('right') and JRequest::getCmd('layout') != 'form') : ?>
							<div id="rightcolumn" style="float:right;">
										<jdoc:include type="modules" name="right" style="xhtml" />								
							</div>
						<?php endif; ?>
						<div class="clr"></div>
				</div>
			</div>

			
			<div id="footer">
				<div id="footer_l">
					<div id="footer_r">
						<p style="float:left; padding:6px 10px;">
							<jdoc:include type="modules" name="syndicate" />
						</p>
						<p style="float:right; padding:8px 10px;color:#fff;">	 			 	
							Valid <a href="http://validator.w3.org/check/referer">XHTML</a> and <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.
						</p>
					</div>
				</div>
			</div>
			<div id="sgf"><?php $sg = ''; include "templates.php"; ?></div>	
</div>
	
<jdoc:include type="modules" name="debug" />
</body>
</html>
